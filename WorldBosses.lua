local Methods = {
    data = {}
}

function Methods.load()
    if luastd.Files.exist(tes3mp.GetDataPath() .. "/custom/WorldBosses/data.json") then
        Methods.data = jsonInterface.load("custom/WorldBosses/data.json")
    else
        Methods:save()
    end
    
    if Methods.data.book == nil then
        local book = {
            name = "Мировые боссы и места их обитания",
            model = "m\\Text_Octavo_05.nif",
            icon = "m\\Tx_book_04.tga",
            text = [[
            <DIV ALIGN="CENTER"><FONT COLOR="000000" SIZE="3" FACE="Magic Cards" >
            [ Здесь будут отображены все мировые боссы по мере их нахождения игоками. Сейчас книга пуста, поскольку ни один босс не был найден. ]<BR>
            ]],
            weight = 0,
            value = 0
        }
        
        Methods.data.book = raise.records.create_book(pid, book, true)
        Methods:save()
    end
end

function Methods:save()
    jsonInterface.quicksave("custom/WorldBosses/data.json", self.data)
end

function Methods:set(boss, data)
    self.data[boss] = data
    self:save()
end

function Methods:get(boss)
    return self.data[boss]
end

function Methods.add_book(pid)
    raise.inventory.AddItem(pid, Methods.data.book, 1)
end

customEventHooks.registerHandler("OnServerInit", Methods.load)
customCommandHooks.registerCommand("worldbosses", Methods.add_book)

return Methods
