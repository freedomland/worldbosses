local messages = {
    "Кто ты и почему пришёл сюда?",
    "Мне совершенно неинтересно разговаривать со смертным.",
    "Сколько вас было таких, кто приходил сюда.",
    "Уходили лишь еденицы.",
    "Ты хочешь битвы? Ха! Только тронь меня, и я уничтожу тебя!",
    "Ты всё ещё здесь?",
    "Уйди прочь, не мешай!"
}
local fake_sunder_msg = "У тебя ненастоящий Разделитель! Какой позор! Ха-ха-ха-ха-ха!"
local fake_keening_msg = "У тебя ненастоящий Разрубатель! Какой позор! Ха-ха-ха-ха-ха!"
local fake_sunder_and_keening_msg = "Ха-ха-ха-ха-ха! Где ты взял эти побрякушки, смертный? Ты правда думаешь, что я бы стал отдавать тебе настоящие инструменты Кагенрака? Только Неревар сможет их найти, не ты. Убирайся прочь, пока я не убил тебя!"

local data = {}

local function on_dagoth_death(eventStatus, pid, cellDescription, refId, count)
    if eventStatus.validCustomHandlers then
        if cellDescription == "Dagoth Ur, Facility Cavern" and refId == data.dagoth_creature then
            TalkyNpc.say_voice_player(pid, "Vo/Misc/Hit_DU003.mp3", "Сдаюсь! Сдаюсь! Ха-ха-ха-ха!")
            mpWidgets.Notification(pid, "5-1-3-1-10 16-15-9-4-17-1-6-13")
            
            local cmds = {
                '"Akula door B"->PlaySound3d "Door Stone Open"',
                '"Akula door B"->PlaySound3d "Door Stone Open"',
                "startscript DagothUrCreature1",
                -- Don't welcome Nerevar (because we're not Nerevar, lol)
                "set DagothUrCreature1.state to 10",
                "set DagothUrCreature1.OpenDoors to 1"
            }
            
            raise.console.run_in_cell(cellDescription, cmds)
        end
    end
end

-- Replace Dagoth in facility cavern
local function raplace_dagoth_cavern()
    data = WorldBosses:get("dagoth")
    
    if not raise.cells.file_exists("Dagoth Ur, Facility Cavern") then
        local location = {               
            posX = 3867.179932,
            posY = 3953.166260,
            posZ = 14107.759766,
            rotX = 0,
            rotY = 0,
            rotZ = 0
        }
        
        local creature = {
            baseId = "dagoth_ur_1",
            script = ""
        }
        
        -- Recreate Dagoth, because I can't disable his welcome's
        if data.dagoth_creature == nil then
            local refId = raise.records.create_creature(pid, creature)
            data.dagoth_creature = refId
            WorldBosses:set("dagoth", data)
        end
        
        raise.cells.insert_actor("Dagoth Ur, Facility Cavern", data.dagoth_creature, location)
    end
end

local function init_facility_cavern(eventStatus, pid, cell)
    if cell == "Dagoth Ur, Facility Cavern" then
        -- Remove Dagoth and Ring of Healing
        raise.objects.delete(pid, cell, "258093-0")
        raise.objects.delete(pid, cell, "425530-0")
            
        local player = Players[pid]
            
        -- Fake Kagenrak instruments, prevent player to move further to main quest
        if tableHelper.containsValue(player.data.inventory, data.fake_keening_id) then
            Players[pid].have_fake_keening = true
        end
                
        if tableHelper.containsValue(player.data.inventory, data.fake_sunder_id) then
            Players[pid].have_fake_sunder = true
        end
    end    
end

local function init_akulahan_chamber(eventStatus, pid, cell)
    if cell == "Akulakhan's Chamber" then
        -- Remove Dagoth
        raise.objects.delete(pid, cell, "262014-0")
            
        -- Remove all dunmers on level, we're going to change them to
        -- another dunmers :D
        raise.objects.delete(pid, cell, "234368-0")
        raise.objects.delete(pid, cell, "234371-0")
        raise.objects.delete(pid, cell, "197602-0")
        raise.objects.delete(pid, cell, "262014-0")
    end
end

local function init_cells(eventStatus, pid, cell)
    if eventStatus.validCustomHandlers then
        init_facility_cavern(eventStatus, pid, cell)
        init_akulahan_chamber(eventStatus, pid, cell)
    end
end

local function fake_instruments(pid)
    local player = Players[pid]
    
    -- Laugth about fake instruments
    if player.have_fake_keening and not player.have_fake_sunder then
        mpWidgets.Notification(pid, fake_keening_msg)
        return
    end
        
    if player.have_fake_sunder and not player.have_fake_keening then
        mpWidgets.Notification(pid, fake_sunder_msg)
        return
    end
        
    if player.have_fake_keening and player.have_fake_sunder then
        mpWidgets.Notification(pid, fake_sunder_and_keening_msg)
        return
    end
    
    TalkyNpc:say_by_script(pid, "dagoth_ur_1", messages)
end

local function on_dagoth_triggered(eventStatus, pid, cell, objects, players)
    if cell == "Dagoth Ur, Facility Cavern" and objects[1].refId == data.dagoth_creature then       
        fake_instruments(pid)
        return customEventHooks.makeEventStatus(false,true)
    end
end

customEventHooks.registerValidator("OnObjectActivate", on_dagoth_triggered)
customEventHooks.registerValidator("OnServerPostInit", raplace_dagoth_cavern)
customEventHooks.registerHandler("OnActorKilled", on_dagoth_death)
customEventHooks.registerHandler("OnCellLoad", init_cells)
